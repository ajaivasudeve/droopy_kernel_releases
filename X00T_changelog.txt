[05-04-2020]
- 0a7594f4 configs: enable kcal color control
- 48abfe15 msm: mdss: kcal: Add KCAL support for post processing control [v2]
- 5ff8b9fe Merge tag 'v4.4.218'

[29-03-2020]
- 51cc8ad1 treewide: Merge tag LA.UM.8.2.r1-06300-sdm660.0
- 07fdcb96 power: revert charger driver changes

[22-03-2020]
- e4efe2f2 Merge tag 'v4.4.217'

[15-03-2020]
- f31153a1 dts: sdm660-overlay: disable hvdcp
- 6e6cb041 dts/power: set max intake current to 2050mA
- 7d6beb07 drivers: power: import qpnp-power-on changes from Asus
- 95cca65a power: remove asus charger driver changes
- 9ba0b28d sched/fair: Don't let tasks slip away from gold to silver cluster
- c3b340c9 kernel/cpu-boost: disable energy awareness for app launches
- 880d1140 sched: use sysctl to control energy aware feature
- 28a604cd dts: smd660: adjust capacity-cost table
- 42f115a2 Merge tag 'v4.4.216'

[08-03-2020]
- d8238701 flasher: post_boot: enable iowait boost
- feb57f9a flasher: post_boot: adjust schedutil rates for quicker transitions
- 18bb8054 msm: mdss: fix inconsistent mutex_lock

[01-03-2020]
- 918540a7 cpu-boost/devfreq_boot: enable max-boosts by default
- ae356666 dts: qcom: adjust devfreq-cpufreq tables
- a0691f49 dts: sdm660: update capacity-cost table
- 7e613516 dts: sdm660: revert cpufreq/devfreq changes
- 93a44439 Merge tag 'v4.4.215'
- 804937fa Revert "ALSA: pcm: Avoid possible info leaks from PCM stream buffers"
- a5e48f7c Revert "ALSA: pcm: Add missing copy ops check before clearing buffer"
- 5e363a42 Revert "usb: dwc3: turn off VBUS when leaving host mode"
- 43602708 treewide: Merge tag LA.UM.8.2.r1-06200-sdm660.0
- ab8b9c26 cpu-boost: mark workqueue WQ_UNBOUND
- 2d006011 devfreq_boot: mark workqueue WQ_UNBOUND
- 2cf4d069 PM: devfreq: Use high priority workqueue
- ef86a8de kgsl: mark workqueues WQ_HIGHPRI

[23-02-2020]
- a15b310f flasher: update AnyKernel3 package to https://github.com/osm0sis/AnyKernel3/commit/c92c8c6972139c156016f145d5e9b9b4f8d5d885
- cadd8ee6 msm: camera: properly initialize vfe
- 2d19f529 Revert "ASoC: tfa9874: Pushing DIN_v6.5.5 to CAF"
- 278b679e mdss_fb: add backlight dimmer option
- 4b76ddd3 Revert "flasher: post_boot: disable dsb for foreground schedgroup"
- 15fb16b1 Revert "flasher: post_boot: adjust cpuset for forground cgroup"
- e43052e9 qpnp-haptic: make its strength adjustable via VibratorHW

[16-02-2020]
- 68068f13 flasher: post_boot: adjust cpuset for forground cgroup
- be5c66cc flasher: post_boot: disable dsb for foreground schedgroup
- 5c328642 Merge tag 'v4.4.214'
- bfdbea8d Merge branch 'kernel.lnx.4.4.r38-rel' from android-linux-stable

[09-02-2020]
- b4d5e0ee Merge tag 'v4.4.213'
- b44bdb4d Revert "Revert "ALSA: pcm: Avoid possible info leaks from PCM stream buffers""
- 3d27d295 treewide: Merge tag LA.UM.8.2.r1-05700-sdm660.0

[02-02-2020]
- 681e483f Revert "kernel: sched: rt: remove rt EAS integration"
- 7fb97cf6 flasher: post_boot: drop boost durations to 150ms
- 92a7c643 Revert "treewide: squash: remove some of kerneltoast's optimizations"
- f0a6f083 Revert "android: lowmemorykiller: checkout to d657433"
- dd8ec29f power: smb-lib: set ICL_2050mA regardless of the charger type
- bc60f18d Merge tag 'v4.4.212'
- e600aded configs: udpate X00T-stock_defconfig
- 335a1552 configs: enable exfat fs
- 881e74a1 fs: include exfat
- be91e82a fs: import exfat fs module
- 62ac30ec fs: remove sdfat fs

[26-01-2020]
- 6613cda0 android: lowmemorykiller: checkout to d657433
- 4ab381b1 treewide: squash: remove some of kerneltoast's optimizations
- febfc0a4 input: fingerprint: cdfingerfp: partial revert to asus import
- eb36d604 Merge tag 'v4.4.211'
- c57e3fb0 flasher: power: adjust lmk minfree series

[19-01-2020]
- 7f7179de circleci: do not log merged commits
- a983da4e flasher: patch: disable adaptive_lmk by default
- cc579eda Merge tag 'v4.4.210'
- 3d0cb4a6 Revert "ALSA: pcm: Avoid possible info leaks from PCM stream buffers"
- c13679b1 Revert "flasher: post_boot: enable iowait_boost"
- 556c0842 devfreq: bw_hwmon: bump up screen_off_max_freq to 762
- 5edf187e qca-wifi-host-cmn: merge tag LA.UM.7.2.r1-08100-sdm660.0
- ea08fe93 qcacld-3.0: merge tag LA.UM.7.2.r1-08100-sdm660.0
- 6e9c651b msm_performance: do not boost if screen is off
- c13ae497 kernel: sched: rt: remove rt EAS integration
- de1caa80 kernel: sched: Mitigate non-boosted tasks preempting boosted tasks
- 8ea73f2a cpuset: Restore tasks affinity while moving across cpusets
- fa820a76 cpu-boost: remove smart_boost modifications
- 68487ff1 power: smb-lib: rework setting intake current limit
- cb99474a Merge tag 'v4.4.209'

[12-01-2020]
- fa58e371 PM / core: Add support to skip power management in device/driver model
- 9705a7b1 topology: silence logging cpu capacity updates
- 41a53103 input: fingerprint: gf_spi: silence logging screen wake
- c2927dfc q6asm: Remove payload size check
- b6d02f61 treewide: bump up max_boost duration
- 2a8f4406 flasher: post_boot: enable iowait_boost
- fd394fe0 input: fingerprint: cdfingerfp: remove figerprint trigger limitations
- a482d816 flasher: post_boot: reconfigure memory parameters
- b5d65e0a drivers: android: remove simple_lmk
- f2bb6dc3 Merge tag 'v4.4.208'

[05-01-2020]
- f9923d80 circleci: correct log period for changelog capture
- f5f40350 configs: disable deadline io-sched
- c13647ad flasher: post_boot: use 762 as min_freq for cpubw
- ffcac21c dts: sdm660: rewrite mincpubw-cpufreq table
- 54022847 dts: sdm660: rewrite memlat tables for both clusters
- 71798e7f Revert "dts: sdm660: adjust cpufreq-devfreq table"
- 806ea1ff dts: sdm660: update capacity-cost model
- 71301c14 dts: sdm660: bridge the gap between initial steps of perfcl
- db9e3974 dts: sdm660: add 300 MHz step to cpufreq table for both clusters
- 8f6ed213 Revert "simple_lmk: reduce ANDROID_SIMPLE_LMK_MINFREE to 84"
- 7d4cfa8a net: add support for TTL fixation
- 05fb2f04 circleci: link "Last Commit" to the actual commit
- 6f4bc995 circleci: use 8 letter abbreviation
- 5e0cafc8 circleci: use tg_notify for weekly build posts
- 219ae3d1 circleci: log changes/commits between builds

[29-12-2019]
- 7e2c5ba8 simple_lmk: reduce ANDROID_SIMPLE_LMK_MINFREE to 84
- 4f1110f9 simple_lmk: revert priority adjustment
- 1828321e cpu-boost/devfreq_boost: disable max-boost by default
- b32189dd flasher: post_boot: increase sb_damp_factor to 10
- 1a244278 flasher: post_boot: cleanup cpu-boost settings
- 30fe3391 circleci: mirror weekly builds to bitbucket
- a39c9e2f circleci: remove brackets [] from $zipname
- fabaa944 circleci: build on push and upload it to a separate chat
- 85e52363 circleci: do not build SAR mounted kernel
- 408b471b circleci: trigger build at 00:30
- 34b4125c schedutil: sb_damp_factor==0 removes damping
- 7067778b Merge branch 'linux-4.4.y' from git.kernel.org
- 0a506855 Revert "smb-lib: cut short setting DCP_CHARGER_CURRENT"

[22-12-2019]
- Initial CI Build